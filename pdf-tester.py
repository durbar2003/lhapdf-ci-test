import lhapdf

# Define a list of parton ID codes to test
parton_ids = range(-6, 7)

# Define a list of (x, Q) tuples to test
test_points = [(0.1, 10), (0.5, 100), (0.9, 1000)]

# Define a tolerance for comparing floating-point values
tolerance = 1e-6

# Load the NNPDF40_nnlo_as_01180 PDF set
pdf_set = lhapdf.getPDFSetByName("NNPDF40_nnlo_as_01180")

# Loop over the parton IDs and test points
for id in parton_ids:
    for x, Q in test_points:
        # Compute the PDF value using LHAPDF
        pdf_value = pdf_set.mkPDF(id).xfxQ(x, Q)

        # Compute the expected PDF value
        expected_value = 0.0 if x == 1 else some_function_of_x_and_Q(id, x, Q)

        # Compare the computed and expected values
        if abs(pdf_value - expected_value) > tolerance:
            raise Exception(f"PDF test failed for parton ID {id}, x={x}, Q={Q}")
